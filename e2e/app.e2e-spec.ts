import { SalmonPage } from './app.po';

describe('salmon App', () => {
  let page: SalmonPage;

  beforeEach(() => {
    page = new SalmonPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
